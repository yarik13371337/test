﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Test.Bl.Abstract.Services;
using Test.Models.Models;

namespace Test.Controllers
{
    public class HouseController : Controller
    {
        private readonly IHouseService _houseService;

        public HouseController(IHouseService houseService)
        {
            _houseService = houseService;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await _houseService.GetAllHouses());
        }

        [HttpGet]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            HouseModel house = await _houseService.GetHouse((int)id);

            if (house == null)
            {
                return NotFound();
            }

            return View(house);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]

        public async Task<IActionResult> Create(HouseModel house)
        {
            await _houseService.CreateHouse(house);
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var house = await _houseService.GetHouse((int)id);

            if (house == null)
            {
                return NotFound();
            }
            return View(house);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, HouseModel house)
        {
            if (id != house.Id)
            {
                return NotFound();
            }

            try
            {
                await _houseService.UpdateHouse(house);
            }
            catch (Exception ex)
            {
                return NotFound();
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            HouseModel house = await _houseService.GetHouse((int)id);

            if (house == null)
            {
                return NotFound();
            }

            return View(house);
        }


        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _houseService.DeleteHouse(id);
            return RedirectToAction(nameof(Index));
        }

    }
}
