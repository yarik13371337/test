﻿using Test.Bl.Abstract.IMapper;
using Test.Entities.Tables;
using Test.Models.Models;

namespace Test.Bl.Impl.Mappers
{
    public class HouseMapper : IBackMapper<House, HouseModel>
    {
        public HouseModel Map(House entity)
        {
            return new HouseModel()
            {
                Id = entity.Id,
                Address = entity.Address,
                Price = entity.Price,
                Square = entity.Square,
            };
        }

        public House MapBack(HouseModel model)
        {
            House house =  new House()
            {
                Address = model.Address,
                Price = model.Price,
                Square = model.Square,
            };
            if (model.Id != null)
                house.Id = (int) model.Id;
            return house;
        }
    }
}
