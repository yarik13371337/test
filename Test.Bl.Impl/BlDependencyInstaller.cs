﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Test.Bl.Abstract.IMapper;
using Test.Bl.Abstract.Services;
using Test.Bl.Impl.Mappers;
using Test.Bl.Impl.Services;
using Test.Entities.Tables;
using Test.Models.Models;

namespace Test.Bl.Impl
{
    public class BlDependencyInstaller
    {
        public static void AddBlServices(IServiceCollection service)
        {
            service.AddTransient<IHouseService, HouseService>();
        }

        public static void AddBlMapper(IServiceCollection service)
        {
            service.AddTransient<IBackMapper<House, HouseModel>, HouseMapper>();
        }
    }
}
