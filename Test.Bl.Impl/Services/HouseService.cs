﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Bl.Abstract.IMapper;
using Test.Bl.Abstract.Services;
using Test.Dal.Abstract.IRepository;
using Test.Entities.Tables;
using Test.Models.Models;

namespace Test.Bl.Impl.Services
{
    public class HouseService : IHouseService
    {
        private readonly IHouseRepository _houseRepository;
        private readonly IBackMapper<House, HouseModel> _houseMapper;
        public HouseService(IHouseRepository houseRepository, IBackMapper<House, HouseModel> houseMapper)
        {
            _houseRepository = houseRepository;
            _houseMapper = houseMapper;
        }
        public async Task<HouseModel> CreateHouse(HouseModel houseModel)
        {
            House result = await _houseRepository.AddAsync(_houseMapper.MapBack(houseModel));
            return _houseMapper.Map(result);
        }

        public async Task DeleteHouse(int id)
        {
            await _houseRepository.DeleteAsync(id);
        }

        public async Task UpdateHouse(HouseModel houseModel)
        {
            await _houseRepository.UpdateAsync(_houseMapper.MapBack(houseModel));
        }

        public async Task<HouseModel> GetHouse(int id)
        {
            House result = await _houseRepository.GetByIdAsync(id);
            return _houseMapper.Map(result);
        }

        public async Task<List<HouseModel>> GetAllHouses()
        {
            List<House> result = await _houseRepository.GetAllAsync(x => true);
            return result.Select(_houseMapper.Map).ToList();
        }
    }
}
