﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Test.Models.Models;

namespace Test.Bl.Abstract.Services
{
    public interface IHouseService
    {
        Task<HouseModel> CreateHouse(HouseModel houseModel);
        Task DeleteHouse(int id);
        Task UpdateHouse(HouseModel houseModel);
        Task<HouseModel> GetHouse(int id);
        Task<List<HouseModel>> GetAllHouses();
    }
}
