﻿namespace Test.Bl.Abstract.IMapper
{
    public interface IBackMapper<TEntity, TModel> : IMapper<TEntity, TModel>
    {
        TEntity MapBack(TModel model);
    }
}
