﻿namespace Test.Bl.Abstract.IMapper
{
    public interface IMapper<TEntity, TModel>
    {
        TModel Map(TEntity entity);
    }
}
