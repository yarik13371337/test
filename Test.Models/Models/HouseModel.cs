﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test.Models.Models
{
    public class HouseModel
    {
        public int? Id { get; set; }
        public decimal Price { get; set; }
        public int Square { get; set; }
        public string Address { get; set; }
    }
}
