﻿using Microsoft.EntityFrameworkCore;
using Test.Entities.Tables;

namespace Test.Dal.Impl.Postgres
{
    public class TestDbContext : DbContext
    {
        public TestDbContext(DbContextOptions<TestDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<House> Houses { get; set; }

    }
}
