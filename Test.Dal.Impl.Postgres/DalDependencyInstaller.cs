﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Test.Dal.Abstract.IRepository;
using Test.Dal.Impl.Postgres.Repository;

namespace Test.Dal.Impl.Postgres
{
    public static class DalDependencyInstaller
    {
        public static void Install(IServiceCollection services)
        {
            services.AddTransient<IHouseRepository, HouseRepository>();
        }
    }
}
