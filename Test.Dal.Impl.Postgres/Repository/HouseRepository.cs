﻿using System;
using System.Collections.Generic;
using System.Text;
using Test.Dal.Abstract.IRepository;
using Test.Dal.Abstract.IRepository.Base;
using Test.Dal.Impl.Postgres.Repository.Base;
using Test.Entities.Tables;

namespace Test.Dal.Impl.Postgres.Repository
{
    public class HouseRepository : GenericKeyRepository<int, House>, IHouseRepository
    {
        public HouseRepository(TestDbContext context) : base(context)
        {

        }
    }
}
