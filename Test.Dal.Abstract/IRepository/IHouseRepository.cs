﻿using System;
using System.Collections.Generic;
using System.Text;
using Test.Dal.Abstract.IRepository.Base;
using Test.Entities.Tables;

namespace Test.Dal.Abstract.IRepository
{
    public interface IHouseRepository : IGenericKeyRepository<int, House>
    {

    }
}
